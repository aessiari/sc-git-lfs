import collections
import enum
from pathlib import Path
import shutil
import tempfile
import time

import pytest
import sys


class DirTestInfo:
    class Action(str, enum.Enum):
        leave_unchanged = 'leave_unchanged'
        modify_content = 'modify_content'
        modify_attrib_only = 'modify_attrib_only'

    def __init__(self):

        self._path = None

        self.files = collections.defaultdict(list)

        self.init_contents()

    @property
    def path(self):
        if self._path is None:
            self._path = Path(tempfile.mkdtemp())
        return self._path

    def init_contents(self):
        for subdir in ['sub_1', 'sub_2', 'sub_3']:
            (self.path / subdir).mkdir()

        file_a = self.path / 'file_a.txt'
        file_b = self.path / 'file_b.txt'
        file_c = self.path / 'file_c.txt'

        for path in [file_a, file_b, file_c]:
            path_to_show = path.relative_to(self.path)
            path.write_text(f'This is {path_to_show}')

        A = self.Action

        self.files[A.modify_content].append(file_a)
        self.files[A.modify_attrib_only].append(file_b)
        self.files[A.leave_unchanged].append(file_c)

    def apply_changes(self):
        A = self.Action

        for path in self.files[A.modify_content]:
            path.write_text('The content was modified')
        for path in self.files[A.modify_attrib_only]:
            path.chmod(0o660)
        for path in self.files[A.leave_unchanged]:
            pass

    def clear(self):
        return shutil.rmtree(self.path)


@pytest.fixture(scope='class')
def test_dir(request):

    dti = DirTestInfo()

    if request.cls:
        request.cls.test_dir = dti

    yield dti

    dti.clear()


@pytest.fixture
def analyzer_configure_opts(test_dir):
    return {
        'monitored_dirs': [test_dir.path],
        'recursive': True
    }


@pytest.fixture(scope='class')
def analyzer(request):
    from sc.capture.sources import dirscan

    ana = dirscan.DirScanAnalyzer()

    request.cls.analyzer = ana
    return ana


def test_files_exist_in_dir(test_dir):
    files = [p for p in test_dir.path.glob('*') if p.is_file()]
    assert files


@pytest.mark.usefixtures('test_dir', 'analyzer')
class TestAnalyzer:

    def test_monitored_dirs_are_set_correctly(self, analyzer_configure_opts):
        self.analyzer.configure(**analyzer_configure_opts)
        assert self.test_dir.path in self.analyzer.monitored_dirs

    def test_dir_content_are_detected_in_scan(self):
        scan_data = self.analyzer.get_dirscan_data()

        assert len(list(scan_data)) == 3

    def get_dirscan_data(self):
        return list(self.analyzer.get_dirscan_data())

    def get_timestamp_now(self):
        return self.analyzer.get_timestamp_now() / 1_000_000_000

    def test_no_events_are_produced_when_timestamp_is_none(self):
        scan_data = self.get_dirscan_data()
        timestamp = None
        events = self.analyzer.get_events_after_timestamp(scan_data, None)
        assert len(events) == 0

    def test_no_events_are_produced_before_applying_changes(self):
        timestamp = self.get_timestamp_now()
        scan_data = self.get_dirscan_data()
        events = self.analyzer.get_events_after_timestamp(scan_data, timestamp)
        assert len(events) == 0

    @pytest.fixture
    def apply_changes_wait_s(self):
        # TODO this could be parametrized if need be to test if there's a minimum value
        return 0.2

    @pytest.fixture
    def events_after_changes(self, apply_changes_wait_s):
        timestamp = self.get_timestamp_now()
        time.sleep(apply_changes_wait_s)
        self.test_dir.apply_changes()
        scan_data = self.get_dirscan_data()
        assert len(scan_data) > 0

        events = self.analyzer.get_events_after_timestamp(scan_data, timestamp)
        return events

    def test_events_are_produced_if_changes_applied_after_timestamp(self, events_after_changes):

        assert len(events_after_changes) > 0

    @pytest.fixture
    def action_events_files_map(self, events_after_changes):
        """
        Return a dict mapping actions to a list of (event, test_file) pair:
        {Action -> [(event, test_file)]}
        """
        A = self.test_dir.Action
        e = events_after_changes
        f = self.test_dir.files

        m = {}

        # TODO this could be made less verbose by storing the event indices only
        # and use that to recreate the list of events for a certain action
        # at the moment it looks a bit overkill so we leave it like this
        m[A.modify_content] = [
            (e[0], f[A.modify_content][0])
        ]

        m[A.modify_attrib_only] = [
            (e[1], f[A.modify_attrib_only][0])
        ]

        return m

    @pytest.mark.skipif(sys.platform != "darwin", reason="does not run on linux")
    def test_events_have_correct_properties(self, action_events_files_map):
        m = action_events_files_map
        A = self.test_dir.Action

        for event, test_path in m[A.modify_content]:
            assert event.action == 'write'
            assert event.artifact.path == str(test_path)

        for event, test_path in m[A.modify_attrib_only]:
            assert event.action == 'modify_attrib'
            assert event.artifact.path == str(test_path)
