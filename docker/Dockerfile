ARG base_image

FROM ${base_image} AS base

# builder_conda: install the `conda` CLI tool
FROM base AS builder_conda

ARG miniconda_version
ARG conda_dir

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV CONDA_DIR=${conda_dir} PATH=${conda_dir}/bin:$PATH

RUN env

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates curl git && \
    apt-get -qq purge && apt-get -qq clean && rm -rf /var/lib/apt/lists/*

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-${miniconda_version}-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p $CONDA_DIR && \
    rm ~/miniconda.sh && \
    $CONDA_DIR/bin/conda clean -tipsy


# installer_sc_system_packages: install system packages that are not too specific to a particular stage (such as e.g. MongoDB packages)
FROM base as installer_system_packages

ARG util_apt_packages="wget curl ncdu x11-apps vim"
# packages needed by the `sc` core package, i.e. not related to event capture
ARG sc_apt_packages="git graphviz vim xdg-utils"
ARG sc_evtcap_apt_packages="psmisc inotify-tools strace lsof libcap2-bin"

RUN apt-get update --fix-missing && \
    apt-get install -y ${sc_apt_packages} ${util_apt_packages} ${sc_evtcap_apt_packages} && \
    apt-get -qq purge && apt-get -qq clean && rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/mikefarah/yq/releases/download/3.3.0/yq_linux_amd64 -O yq && \
    chmod +x yq && \
    mv yq /usr/local/bin/


# builder_sc_python_package: build the `sc` python package and install it in its own Conda environment
FROM builder_conda as builder_sc_conda_env

RUN env

ARG conda_env_source_file
ARG build_dir="/tmp"

ARG sc_env_name="sc"
# might want to move these to the env file as well, if we use only one file anyway

ARG sc_env_dir=$CONDA_DIR/envs/${sc_env_name}

WORKDIR ${build_dir}

# install dependencies using Conda
COPY ${conda_env_source_file} ./dependencies.yml
RUN conda env create --file dependencies.yml && conda clean -afy

COPY ["setup.py", "VERSION", "./"]

ARG python_package_source_dir
ARG python_package_dir=${python_package_source_dir}

COPY ${python_package_source_dir}/ ${python_package_dir}/
# `conda run` is available in recent conda versions to run a command in a conda env without having to set the $PATH manually
RUN conda run --name ${sc_env_name} python -m pip install .

ARG ui_source_dir
ARG ui_dir="$CONDA_DIR/envs/sc/lib/python3.7/site-packages/ui"

COPY ${ui_source_dir}/ ${ui_dir}/
# install NPM packages for UI
RUN cd ${ui_dir}/backend && conda run --name ${sc_env_name} npm install


# sc_base_assembler: copy built packages from other build containers, copy other data-only directories
# to assemble the Science Capsule base image
FROM installer_system_packages as sc_base_assembler


ARG sc_user=capsule
ARG sc_uid=1000

ENV USER ${sc_user}
ENV HOME /home/capsule

RUN groupadd \
        --gid ${sc_uid} \
        ${sc_user} && \
    useradd \
        --comment "Science Capsule user" \
        --create-home \
        --gid ${sc_uid} \
        --uid ${sc_uid} \
        --no-log-init \
        --shell /bin/bash \
        ${sc_user}

ARG conda_dir
ENV CONDA_DIR=${conda_dir}
# TODO this also copies downloaded packages and other things we don't need, so it would be good to do a more selective copy
COPY --chown=${sc_user}:${sc_user} --from=builder_sc_conda_env $CONDA_DIR $CONDA_DIR

COPY "./docker/entrypoint.sh" /
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

ARG sc_exes="sc-bootstrap sc sc-snapshot"
ARG allenvs_bin_dir=/usr/local/bin

# in the printf string we have to hardcode the the value of {conda_dir} or it causes conflics with printf
RUN for exe in ${sc_exes}; do printf '#!/bin/bash\n\n. $CONDA_DIR/etc/profile.d/conda.sh && conda activate sc\n\n$CONDA_DIR/envs/sc/bin/%s "$@"\n' $exe > ${allenvs_bin_dir}/$exe; done && \
    chmod -R +x ${allenvs_bin_dir} && \
    ln -s $CONDA_DIR/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". $CONDA_DIR/etc/profile.d/conda.sh" >> $HOME/.bashrc && \
    echo "conda activate base" >> $HOME/.bashrc

USER ${sc_user}
WORKDIR ${HOME}

ARG sc_config_dir
ENV SC_CONFIG_DIR=${sc_config_dir}

ENV PYTHONUNBUFFERED=1

RUN sc-bootstrap $SC_CONFIG_DIR --monitored-dir . -s inotify
# RUN /opt/conda/bin/conda run --name sc sc-bootstrap .scicap -d workdir
