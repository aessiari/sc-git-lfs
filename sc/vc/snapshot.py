from git import Repo
from git.exc import InvalidGitRepositoryError, GitCommandError
import os
import time
import uuid
from tabulate import tabulate

from sc.db.sqlite import SqlStore
from sc.db.schema import SnapshotsDatabase
from sc.config import CONFIG_DIR
from sc.driver import Driver
from sc.constants import CapsuleView
from sc.db.mongo import MongoStore
from sc.db.schema import MetadataDatabase


class TimeCapsule(object):
    def __init__(self, repo_path, archive_dir):
        if not os.path.exists(CONFIG_DIR):
            driver = Driver()
            driver.setup()
        try:
            self.repo = Repo(repo_path, search_parent_directories=True)
        except InvalidGitRepositoryError as e:
            self.repo = Repo.init(repo_path)
        self.repo.config_writer().set_value("core", "excludesfile",
                                            os.path.join(CONFIG_DIR, 'sc-snapshot.ignore')).release()
        if not os.path.exists(os.path.join(CONFIG_DIR, SnapshotsDatabase.DB)):
            self.snapshot_store = SqlStore()
            self.snapshot_store.create()
        else:
            self.snapshot_store = SqlStore()
        #self.metadata_db = get_connection()
        self.metadata_db = MongoStore()
        self.metadata_db.connect(MetadataDatabase.DB)
        self.archive_dir = archive_dir # TODO: move to a DB

    def create_archive(self):
        # TODO: get the current hash of the repo and set that the archive name
        archive = str(uuid.uuid4()) + '.tar'
        archive_file = os.path.join(self.archive_dir, archive)
        with open(archive_file, 'wb') as fp:
            self.repo.archive(fp) 
        return archive

    def __get_tracked_changes__(self, index, branch=None):
        diff_index = index.diff(branch)

        change_type_map = {'M': 'Modified', 'A': 'Added',
                           'D': 'Deleted', 'R': 'Renamed',
                           'T': 'Changed type'}

        changes = {}
        for change_type in change_type_map:
            changes[change_type] = []
            for diff_item in diff_index.iter_change_type(change_type):
                if change_type == 'R':
                    changes[change_type].append(diff_item.raw_rename_to)
                else:
                    changes[change_type].append(diff_item.a_path)

        return changes


    def create_snapshot(self, name):
        objects = export.get_provevts_for_ingest_clients(convert=export._serialize_for_ingest_client)
        driver = Driver()
        driver.ingest(objects)
        try:
            untracked_files = self.repo.untracked_files
            if self.repo.is_dirty() or len(untracked_files) > 0:
                index = self.repo.index
                if self.repo.is_dirty():
                    changes = self.__get_tracked_changes__(index)
                    for ctype in changes:
                        if ctype == 'D' and len(changes[ctype]) > 0:
                            index.remove(changes[ctype])
                        else:
                            index.add(changes[ctype])
                if len(untracked_files) > 0:
                    index.add(untracked_files)
                index.commit("Adding changes to the snapshot")
                t = self.repo.create_tag(name)
                # name, alias, time, parent, branch
                branch = str(self.repo.active_branch)
                snapshot_time = t.commit.committed_datetime
                n_tags = self.snapshot_store.get_num_snapshots()
                alias = n_tags
                parent = self.snapshot_store.get_current_snapshot(branch)
                rec = [(name, alias, snapshot_time, parent, branch)]
                self.snapshot_store.put(rec)
                return t
            else:
                print("There is nothing new to snapshot")
                return None
        except Exception as e:
            print('Error creating snapshot {}: {}'.format(name, e))


    def get_snapshot(self, name):
        print('SC: get_snapshot')
        untracked_files = self.repo.untracked_files
        if self.repo.is_dirty() or len(untracked_files) > 0:
            print("Taking snapshot of the current workspace")
            timestamp = int(round(time.time() * 1000))
            current_snapshot_name = "sc-snapshot-" + str(timestamp)
            self.create_snapshot(current_snapshot_name)
            print("Snapshot `{}` created".format(current_snapshot_name))
        print("Getting snapshot: {}".format(name))
        timestamp = int(round(time.time() * 1000))
        snapshot_branch = self.snapshot_store.get_snapshot_branch(name)
        if snapshot_branch is None:
            print("Snapshot {} does not exist".format(name))
            return None
        latest_branch_snapshot = self.snapshot_store.get_current_snapshot(snapshot_branch)
        if latest_branch_snapshot == name:
            self.repo.git.checkout(snapshot_branch)
            return snapshot_branch
        else:
            branch_name = name + '-' + str(timestamp)
            self.repo.git.checkout('-b', branch_name, name)
            self.snapshot_store.put_branch_snapshot(branch_name, name)
            return branch_name


    # TODO: needs to be fixed since deleted snapshots can be referenced back
    def remove_snapshot(self, names):
        print('SC: remove_snapshot')
        try:
            for name in names:
                branch = self.snapshot_store.remove_snapshot_info(name)
                if branch is not None:
                    self.repo.delete_head(branch, force=True)
                else:
                    self.repo.delete_tag(name)
        except GitCommandError as e:
            print("Unable to remove snapshot as it is the current workspace")


    def list_snapshots(self):
        snapshots = reversed(sorted(self.repo.tags, key=lambda t: t.commit.committed_datetime))
        snapshot_list = []
        for tag in snapshots:
            #print("{} {}".format(tag.name, str(tag.commit.committed_datetime)))
            snapshot_list.append([tag.name, str(tag.commit.committed_datetime)])

        header = ['Snapshot', 'Date']

        print('SC: list_snapshots')
        print(tabulate(snapshot_list, headers=header))

        return snapshots


    def check_snapshot(self, tag, verbose=False):
        commit = self.repo.commit(tag)
        #print(commit.stats.files)
        #print(commit.hexsha)

        print('SC: check_snapshot')
        obj_info = []
        if verbose:
            obj_info = self.__verbose_check_snapshot__(commit)
        else:
            files = self.repo.git.execute(
                ['git', 'ls-tree', '--name-only', commit.hexsha]).split()
            header = ['Filename']
            for f in files:
                obj_info.append([f])
            print(tabulate(obj_info, headers=header))

        return obj_info

    def __verbose_check_snapshot__(self, commit):
        blob_commits = self.repo.git.execute(
            ['git', 'ls-tree', commit.hexsha]).split('\n')

        commits = []
        commit_map = {}
        # get commit details for every file
        for blob in blob_commits:
            info = blob.split('\t')
            hexsha = info[0].split()[-1]
            commits.append(hexsha)
            if hexsha in commit_map:
                commit_map[hexsha].append(info[1])
            else:
                commit_map[hexsha] = [info[1]]
            
        obj_info = []
        for k in commit_map:
            author_name = commit.author.name
            authored_date = commit.authored_date
            for fname in commit_map[k]:
                obj_info.append([fname,
                                 author_name,
                                 time.strftime("%d %b %Y %H:%M", time.gmtime(authored_date))])
            
        header = ['Filename', 'Author', 'Date']

        print(tabulate(obj_info, headers=header))

        return obj_info


    def snapshot_diff(self, snap2, snap1='master'):
        snap1_commit = self.repo.commit(snap1)
        snap2_commit = self.repo.commit(snap2)
        diff_index = snap2_commit.diff(snap1_commit)

        change_type_map = {'M': 'Modified', 'A': 'Added',
                           'D': 'Deleted', 'R': 'Renamed',
                           'T': 'Changed type'}

        changes = []
        for change_type in change_type_map:
            #print('{}'.format(change_type_map[change_type]))
            for diff_item in diff_index.iter_change_type(change_type):
                if change_type == 'R':
                    #print('\t{} -> {}'.format(diff_item.raw_rename_from,
                    #                          diff_item.raw_rename_to))
                    changes.append([change_type_map[change_type],
                                    '{} -> {}'.format(diff_item.raw_rename_from,
                                                      diff_item.raw_rename_to),
                                    diff_item.score])
                else:
                    #print('\t{}'.format(diff_item.a_rawpath))
                    changes.append([change_type_map[change_type],
                                    diff_item.a_rawpath,
                                    diff_item.score])

        #for diff_item in diff_index.iter_change_type('M'):
        #    print("Modified: {}".format(diff_item.a_rawpath))
        #for diff_item in diff_index.iter_change_type('A'):
        #    print("Added: {}".format(diff_item.a_rawpath))
        #for diff_item in diff_index.iter_change_type('D'):
        #    print("Deleted: {}".format(diff_item.a_rawpath))
        #for diff_item in diff_index.iter_change_type('R'):
        #    print("Renamed: {}".format(diff_item.a_rawpath, diff_item.b_rawpath))
        #for diff_item in diff_index.iter_change_type('T'):
        #    print("Changed type: {}".format(diff_item.a_rawpath))

        header = ['Category', 'Files', 'Score']

        print('SC: snapshot_diff')
        print(tabulate(changes, headers=header))

        return diff_index

    def get_view(self, name, view, outfile, fmt='svg'):
        snapshot_ids = self.snapshot_store.recurse_snapshot_ids(name)
        if view == CapsuleView.PROVENANCE:
            objects = self.metadata_db.get_objects_by_snapshot_id(snapshot_ids)
            g = provenance.build_provenance_graph(objects)
        else:
            objects_timeline = self.metadata_db.get_timeline_by_snapshot_id(snapshot_ids)
            g = timeline.build_timeline(objects_timeline)
        dot = g.graphviz_graph(fmt)
        g.visualize(dot, outfile)

    def shutdown(self):
        self.snapshot_store.close()


if __name__ == '__main__':
    tc = TimeCapsule('/Users/DGhoshal/workdir/sciencecapsule/gittest',
                     '/Users/DGhoshal/workdir/sciencecapsule/gittest')
    tc.create_archive()
    tc.create_snapshot('0.05')
    tc.check_snapshot('0.1')
    tc.list_snapshots()
    tc.snapshot_diff('0.2', '0.05')
    tc.check_snapshot('0.05')
