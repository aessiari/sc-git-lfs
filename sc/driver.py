import logging
import os

from sc.constants import CapsuleView
from sc.config import CONFIG_DIR
from sc.db.sqlite import SqlStore
from sc.db.mongo import MongoStore
from sc.db.schema import SnapshotsDatabase
from sc.db.schema import MetadataDatabase


LOG = logging.getLogger(__name__)


class Driver(object):
    def __init__(self):
        #self.db = get_connection()
        self.db = MongoStore()
        self.db.connect(MetadataDatabase.DB)
        # self.sc_id = capsule_id
        if not os.path.exists(os.path.join(CONFIG_DIR, SnapshotsDatabase.DB)):
            self.snapshot_store = SqlStore()
            self.snapshot_store.create()
        else:
            self.snapshot_store = SqlStore()

    def setup(self, dir_name=None):
        LOG.info(f'Running setup for {CONFIG_DIR}')
        if not os.path.exists(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)

        #if dir_name is None:
        #    dir_name = os.getcwd()
        #print("Monitoring: {}".format(dir_name))

        ignore_file = os.path.expandvars(os.path.join(CONFIG_DIR, 'sc-snapshot.ignore'))
        if not os.path.exists(ignore_file):
            ignore_list = ['.scconfig', '*.com', '*.class', '*.dll', '*.exe', '*.o', '*.so',
                           '*.7z', '*.dmg', '*.gz', '*.iso', '*.jar', '*.rar', '*.tar', '*.zip',
                           '*.log', '.DS_Store', '.DS_Store?', '._*', '.Spotlight-V100', '.Trashes',
                           '__pycache__', '.AppleDouble', '.pyc', '*-egg-info', 'ehthumbs.db',
                           'Thumbs.db', '*~', '*.swp', '*.swo', '\#*\#', '.\#*']
            with open(ignore_file, 'w') as f:
                for item in ignore_list:
                    f.write(item + os.linesep)

    def ingest(self, objects):
        if not os.path.exists(CONFIG_DIR):
            self.setup()

        n_tags = str(self.snapshot_store.get_num_snapshots())
        self.db.insert_object_metadata(objects, snapshot_id=n_tags)
        LOG.info('Ingested {} events'.format(len(objects)))

    def cleanup(self):
        self.db.truncate_metadata()
