from sc.logging import LoggingMixin
from sc.db import models
from sc.capture import exceptions


class _EventProducer(LoggingMixin):
    # this is just a stand-in for the real event model
    event_model = models.RawEvent

    def __init__(self, *args, source=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._source = source

    @property
    def source(self):
        return self._source

    @property
    def event_manager(self):
        return self.event_model.objects


class Monitor(_EventProducer):
    """
    A real-time event producer that runs continuously, exposing a `commit()` method
    to persist the events.
    """

    def start(self):
        ...

    def stop(self):
        ...

    # TODO this would be better as a proper Monitor state-machine like API,
    # similarly to what Supervisor is doing at the service level
    def check_status(self):
        # we assume that the monitor is running fine unless errors are explicitly raised
        ...

    @property
    def event_manager(self):
        return self.event_model.staged

    def commit(self):
        # we can keep this thin, and assume that the commentary is done on the other end
        return self.event_manager.commit()


class Ingester(_EventProducer):
    """
    An event producer responsible for generating and saving events.
    Subclasses should implement the run() method that will be called to trigger the ingestion
    and return the saved events.
    """

    def run(self, **kwargs):
        events = []
        self.save(events)
        return events

    def save(self, events, populate_defaults=True, **kwargs):
        n_events = len(events)
        if n_events == 0:
            self.log.debug('No events to save')
            return None

        opts = {}
        if populate_defaults:
            # full_clean defaults to True for Model().save(), but to False for Model.objects.bulk_create()
            # the validation part is not essential at this stage but it's needed
            # to populate each model instance's attributes from the field's default value
            opts['full_clean'] = True
        try:
            result = self.event_manager.bulk_create(events, **opts)
        # TODO use more specific exceptions?
        except Exception as e:
            raise exceptions.DatabaseSaveError from e
        else:
            self.log.debug('Saved %d events', n_events)
            n_inserted = len(result)

        return n_inserted


class Adaptor:
    source = None
    concerns = set()

    REGISTRY = {}

    def __init_subclass__(cls):
        Adaptor._register(cls)

    @classmethod
    def _register(cls, subclass, source=None):
        source = source or subclass.source
        cls.REGISTRY[source] = subclass

    def configure(self, settings=None):
        ...
