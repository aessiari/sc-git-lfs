import contextlib
import logging
from pathlib import Path
import time

import yaml

from sc.db import models
from sc.capture import exceptions
from sc.capture.base import Adaptor
from sc.capture import sources as sources_module


LOG = logging.getLogger(__name__)


class Settings:

    @classmethod
    def from_config(cls):
        from sc.config import CONFIG_DIR

        config_file_path = CONFIG_DIR / 'events.yml'

        return cls.from_file(config_file_path)

    @classmethod
    def from_file(cls, path):
        path = Path(path)

        LOG.info('Initializing capture settings from file %s', path)

        with path.open() as f:
            data = yaml.safe_load(f)

        return cls(data)

    def __init__(self, data=None):

        self._data = data or {}

    @property
    def monitored_dirs(self):
        valid = []

        for item in self._data.get('monitored_dirs', []):
            try:
                path = Path(item).absolute()
                assert path.is_dir()
            except Exception as e:
                raise exceptions.ConfigurationError('"%s" is not a valid directory') from e
            else:
                valid.append(path)

        return valid

    @property
    def recursive(self):
        return self._data.get('recursive', True)

    @property
    def monitored_pids(self):
        return self._data.get('monitored_pids', [])
        # pids_spec = self._data.get('monitored_pids', None)
        # if not pids_spec:
        #     return []
        # pids_env_var_name = "SC_MONITORED_PIDS"
        # if pids_spec == pids_env_var_name:
        #     spec_from_env = os.environ.get(pids_env_var_name, '')
        #     pids = spec_from_env.split(',')

    @property
    def sources(self):
        return self._data.get('sources', [])


class CaptureManager:

    def __init__(self):
        # self.sources = []
        self.adaptors = []

        self._with_running_monitor = []

    def __iter__(self):
        return iter(self.adaptors)

    def __len__(self):
        return len(self.adaptors)

    @property
    def with_monitor(self):
        return [a for a in self if hasattr(a, 'monitor')]

    @property
    def with_ingester(self):
        return [a for a in self if hasattr(a, 'ingester')]

    @property
    def with_running_monitor(self):
        return list(self._with_running_monitor)

    def init_sources(self, sources, settings):
        for source in sources:
            LOG.info('Loading requested source "%s"', source)

            # TODO add error handling for e.g. non-existing sources?
            try:
                adaptor = Adaptor.REGISTRY[source]()
                adaptor.configure(settings)
            except KeyError as e:
                raise exceptions.ConfigurationError(f'No registered adaptor found for requested source "{source}"') from e
            except Exception as e:
                # LOG.exception(e)
                raise exceptions.ConfigurationError(f'Error while configuring source "{source}"') from e
            else:
                self.adaptors.append(adaptor)

        LOG.debug('monitors:')
        for a in self.with_monitor:
            LOG.debug('\t%s', a.monitor)
        LOG.debug('ingesters:')
        for a in self.with_ingester:
            LOG.debug('\t%s', a.ingester)

    # TODO this should not probably happen here, unless we move the lower-level database
    # functionality within the sources/adaptor themselves
    # def purge(self):
    #     to_purge = [models.EventProcessingLog.objects.all()]
    #     for monitor in self.monitors:
    #         to_purge.append(monitor.events.all())

    #     for queryset in to_purge:
    #         db_model = queryset._model
    #         LOG.info('Deleting events for model: %s', db_model)
    #         LOG.debug('model: %s, count: %d', db_model, queryset.count())
    #         queryset.delete()
    #         LOG.debug('model: %s, count: %d', db_model, queryset.count())

    def capture(self, ingest=True, display_events=False, save_adapted_events=True):
        all_events = []

        to_analyze = set()
        to_skip = set()

        for adaptor in self.with_running_monitor:
            monitor = adaptor.monitor
            try:
                monitor.check_status()
                committed = monitor.commit()
            except exceptions.MonitorStatusException as e:
                LOG.error('Encountered issue(s) with monitor for adaptor %s', adaptor)
                LOG.exception(e)
                self._manage_monitor_error(adaptor)
                to_skip.add(adaptor)
            else:
                adaptor_group = to_analyze if committed else to_skip
                adaptor_group.add(adaptor)

        if to_analyze:
            LOG.debug(to_analyze)

        if ingest:
            ingested_events = self.ingest(to_skip=to_skip)
            all_events.extend(ingested_events)

        if display_events:
            self.display_events(all_events)

        if save_adapted_events:
            self.populate_with_current_ui_schema(all_events)

        return all_events

    def display_events(self, events):
        for event in events:
            LOG.debug(repr(event))

    def ingest(self, to_skip=None):
        all_events = []
        to_skip = to_skip or set()

        for adaptor in self.with_ingester:
            if adaptor in to_skip:
                continue
            LOG.info('Running ingester from adaptor %s', adaptor)
            events = adaptor.ingester.run()
            LOG.info('Created %d event(s)', len(events))
            all_events.extend(events)

        return all_events

    def populate_with_current_ui_schema(self, events):
        if not events:
            return
        n_events = len(events)
        what = 'events adapted to the current UI schema'

        curr_ui_model = models.CurrentUIEvent
        LOG.info('Creating %s from %d events', what, n_events)

        adapted_events = [
            curr_ui_model.from_event(event)
            for event in events
        ]

        try:
            ids_saved = curr_ui_model.objects.bulk_create(
                adapted_events,
                full_clean=True
            )
            n_saved = len(ids_saved)
        except Exception as e:
            LOG.warning('Could not save %d %s: %s', n_events, what, e)
        else:
            LOG.info('Saved %d %s', n_saved, what)
            return n_saved

    @contextlib.contextmanager
    def setup_monitoring(self):
        self.start_monitoring()

        try:
            yield
        except KeyboardInterrupt:
            LOG.info('Received keyboard interrupt')
        except exceptions.ReceivedStopSignal as e:
            LOG.info('Received stop signal (%d)', e.sig_num)
        finally:
            LOG.info('Stopping monitoring')
            self.stop_monitoring()
            LOG.info('Monitoring complete.')

    def start_monitoring(self):
        for adaptor in self.with_monitor:
            # TODO check if monitor.start() succeeds before adding to list of running monitors
            adaptor.monitor.start()
            self._with_running_monitor.append(adaptor)

    def stop_monitoring(self):
        # TODO general timeout option for stop()?
        for adaptor in self.with_running_monitor:
            adaptor.monitor.stop()

    def run_forever(self, timeout_s=3, **kwargs):
        while True:
            try:
                time.sleep(timeout_s)
                self.capture(**kwargs)
            except Exception as e:
                LOG.info('Received exception %s', e)
                raise

    def _manage_monitor_error(self, adaptor):
        source = adaptor.source
        LOG.info('Removing monitor for source "%s" from list of running monitors', source)
        self._with_running_monitor.remove(adaptor)
        LOG.info('Removing adaptor for source "%s" from list of loaded adaptors', source)
        self.adaptors.remove(adaptor)
        LOG.info('Loaded adaptors are now: %s', self.adaptors)
        LOG.info('With running monitor: %s',  self.with_running_monitor)
