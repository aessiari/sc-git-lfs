import collections
import datetime as dt
import os
import time

import pymongo
import pymodm

from sc.capture import base, exceptions
from sc.db import models


SOURCE = 'dirscan'


# TODO check if there isn't a built-in recursive option
def scantree(path, recursive=False, **kwargs):
    for entry in os.scandir(path):
        if entry.is_dir(**kwargs):
            if not recursive:
                continue
            else:
                yield from scantree(entry.path)
        else:
            yield entry


class FileInfo(pymodm.EmbeddedMongoModel):
    path = pymodm.CharField()
    ctime = pymodm.FloatField()
    mtime = pymodm.FloatField()

    @classmethod
    def from_scandir_entry(cls, entry):
        stats = entry.stat()

        return cls(
            path=entry.path,
            ctime=stats.st_ctime,
            mtime=stats.st_mtime
        )


class DirScan(models.RawEvent):
    entries = pymodm.EmbeddedDocumentListField(FileInfo, required=False)

    objects = models.UtilManager()


class DirScanAnalyzer(base.Ingester):
    raw_event_model = DirScan
    event_model = models.FilesystemEvent

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.monitored_dirs = []
        self.recursive = None

    def configure(self, monitored_dirs, recursive=False):
        self.monitored_dirs = monitored_dirs
        self.recursive = recursive

    def get_timestamp_now(self):
        return time.time_ns()

    def get_timestamp_last_capture(self, unit='s'):
        converters_from_ns = {
            's': lambda x: x / 1_000_000_000,
            'ns': lambda x: x
        }
        converter = converters_from_ns[unit]

        last = self.raw_event_model.objects.all().get_last_inserted()
        if last is not None:
            return converter(last.timestamp_ns)

    def get_dirscan_data(self):

        for dir_path in self.monitored_dirs:
            for entry in scantree(dir_path):
                yield FileInfo.from_scandir_entry(entry)

    def get_events_after_timestamp(self, dirscan_data, timestamp):
        events = []

        if not timestamp:
            return events

        for file_info in dirscan_data:
            print(f'file_info.path={file_info.path}')
            print(f'timestamp={timestamp}')
            print(f'file_info.ctime={file_info.ctime}')
            if any(ts > timestamp for ts in [file_info.ctime, file_info.mtime]):
                event = self.create_event(file_info)
                events.append(event)

        return events

    def create_event(self, file_info):
        event = self.event_model(
            time=dt.datetime.utcfromtimestamp(file_info.ctime)
        )
        fs = models.filesystem
        event.artifact.type = fs.ArtifactType.file
        event.artifact.path = file_info.path
        # event.artifact = {
        #     'type': fs.ArtifactType.file,
        #     'path': file_info.path
        # }

        if file_info.ctime == file_info.mtime:
            event.action = fs.Action.write
        else:
            event.action = fs.Action.modify_attrib

        return event

    def run(self, **kwargs):
        timestamp_last_capture = self.get_timestamp_last_capture()
        # since in the current impl this is an iterator,
        # we load this in memory to be able to use it twice
        dirscan_data = list(self.get_dirscan_data())
        events = self.get_events_after_timestamp(dirscan_data, timestamp_last_capture)
        self.save(events)
        self.commit(dirscan_data)
        return events

    def commit(self, dirscan_data):
        # at the moment we're only saving the capture timestamp,
        # but we could also save the dirscan data (full or partial)
        self.raw_event_model.objects.create(
            timestamp_ns=self.get_timestamp_now()
        )


class Adaptor(base.Adaptor):
    source = SOURCE

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ingester = DirScanAnalyzer()

    def configure(self, settings):
        self.ingester.configure(
            monitored_dirs=settings.monitored_dirs,
            recursive=settings.recursive
        )
