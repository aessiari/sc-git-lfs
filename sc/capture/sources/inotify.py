import os

from sc.db import models
from sc.capture import base, monitors, analysis, util


SOURCE = 'inotify'


class InotifyRawEvent(models.RawEvent):
    "Event produced by capturing output of `inotifywait`"

    event_data = models.fields.CharField(blank=True, required=True)

    objects = models.UtilManager()
    staged = models.StagingManager()
    incremental = models.IncrementalManager(range_field='timestamp_ns')


class Monitor(monitors.SubprocessOutputMonitor):
    event_model = InotifyRawEvent

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.snippets_for_discarding = []

    def configure(self, monitored_dirs=None, recursive=None, ignore_config_dir=True):
        args = ['inotifywait']

        args += ['--monitor']
        args += ['--csv']

        if recursive:
            args.append('--recursive')

        monitored_dirs = monitored_dirs or []
        assert monitored_dirs, 'At least one monitored dir should be given'
        # TODO this call to fspath() is needed because of pathlib.Path instances
        # causing errors on Windows when used as subprocess.Popen args
        # consider if we want to use the same CmdArgs as we're using for sc-bootstrap
        args.extend([os.fspath(p.absolute()) for p in monitored_dirs])

        dir_access_flags = ['OPEN,ISDIR', 'ACCESS,ISDIR', 'CLOSE_NOWRITE,CLOSE,ISDIR']
        dirs_to_ignore = []

        if ignore_config_dir:
            from sc.config import CONFIG_DIR

            dirs_to_ignore.append(CONFIG_DIR)

        self.cli_args = args
        self.snippets_for_discarding = dir_access_flags + [os.fspath(d.absolute()) for d in dirs_to_ignore]

    def is_to_discard(self, line):
        return any([snippet in line for snippet in self.snippets_for_discarding])

    def process_line(self, line):
        if self.is_to_discard(line):
            return

        return super().process_line(line)


class RawEventsProcessingPipeline(analysis.LinearPipeline):

    @property
    def event_cls(self):
        return self.params.get('event_cls')

    @property
    def default_steps(self):
        return [
            self.merge_csv_column,
            self.upcast_dtypes,
            self.drop_raw_fields,
            self.coalesce_repeated,
            self.assign_event_properties,
            self.remove_spurious,
            self.create_event_objects,
        ]

    class c:
        """
        Convenience shortcut to provide a namespaced single source for the internal field names
        """
        timestamp_ns = 'timestamp_ns'
        event_data = 'event_data'
        path_root = 'path_root'
        flags = 'flags'
        path_item = 'path_item'
        csv_fields = [path_root, flags, path_item]
        path = 'path'
        event_type = 'event_type'
        ts = 'ts'
        action = 'action'
        artifact_type = 'artifact_type'

    def merge_csv_column(self, df):
        return (df
                .pipe(
                    util.merge_csv_column,
                    self.c.event_data,
                    names=self.c.csv_fields
                )
                .fillna('')
               )

    def upcast_dtypes(self, df):

        def convert_timestamp(s):
            return util.get_timestamp_from_epoch(s, unit='ns')

        assigns = {
            self.c.path: lambda d: d[self.c.path_root] + d[self.c.path_item],
            self.c.ts: lambda d: d[self.c.timestamp_ns].pipe(convert_timestamp)
        }

        return (df
                .assign(**assigns)
               )

    def drop_raw_fields(self, df):
        to_drop = [
            self.c.event_data,
            self.c.path_root,
            self.c.path_item,
            self.c.timestamp_ns,
        ]

        return df.drop(columns=to_drop)

    def coalesce_repeated(self, df):
        coalesce = util.CoalesceAdjacent(
            distinct=[self.c.path, self.c.flags],
            timestamp_column=self.c.ts,
            timedelta_max=0.2
        )

        return (df
                .sort_values([self.c.path, self.c.ts])
                .pipe(coalesce)
                .sort_values(self.c.ts)
               )

    def assign_event_properties(self, df):

        # we copy the df since we're setting values directly
        df = df.copy()

        # TODO the conditional assignment here could be streamlined with a sequence of
        # (condition, assignment), where assignment is a map {col: val} as in df.assign()
        flags = df[self.c.flags]

        fs = models.filesystem

        df.loc[flags.str.contains('CREATE'), self.c.action] = fs.Action.create
        df.loc[flags.str.contains('DELETE'), self.c.action] = fs.Action.delete
        df.loc[flags.str.contains('ATTRIB'), self.c.action] = fs.Action.modify_attrib
        df.loc[flags.str.contains('ACCESS'), self.c.action] = fs.Action.read
        df.loc[flags.str.contains('MOVED'), self.c.action] = fs.Action.move
        df.loc[flags.str.contains('MODIFY'), self.c.action] = fs.Action.write

        is_dir = flags.str.contains('ISDIR')

        df.loc[is_dir, self.c.artifact_type] = fs.ArtifactType.directory
        df.loc[~is_dir, self.c.artifact_type] = fs.ArtifactType.file

        return df.astype({
            self.c.action: object,
            self.c.artifact_type: object
        })

    def remove_spurious(self, df):
        fields_requiring_value = [
            self.c.path,
            self.c.action,
            self.c.artifact_type
        ]

        return (df
                .dropna(subset=fields_requiring_value)
               )

    def get_event_from_dataframe_row(self, row, event_cls=None):
        # NOTE in principle event_cls could also be decided here, depending on the data
        e = event_cls()

        e.time = row[self.c.ts]
        # e.interval_type = row[self.c.position_in_chunk]
        e.duration = row['duration'].total_seconds()

        e.action = row[self.c.action]
        e.artifact_type = row[self.c.artifact_type]

        # e.artifact = {
        #     'type': e.artifact_type,
        #     'path': row[self.c.path]
        # }
        e.artifact.type = e.artifact_type
        e.artifact.path = row[self.c.path]

        # TODO add field marking that this event was produced by 'inotify'
        e.source = SOURCE

        return e

    def create_event_objects(self, df):
        return util.create_objects_from_dataframe(
            df,
            self.get_event_from_dataframe_row,
            event_cls=self.event_cls
        )


# TODO if this is so minimal, maybe it could be generated automatically?
class Analyzer(analysis.RawEventsAnalyzer):
    raw_event_model = InotifyRawEvent
    event_model = models.FilesystemEvent

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.processor = RawEventsProcessingPipeline(event_cls=self.event_model)


class Adaptor(base.Adaptor):
    source = SOURCE

    def __init__(self, *args, monitor=None, ingester=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.monitor = monitor or Monitor()
        self.ingester = ingester or Analyzer()

    def configure(self, settings):
        self.monitor.configure(
            monitored_dirs=settings.monitored_dirs,
            recursive=settings.recursive
        )
