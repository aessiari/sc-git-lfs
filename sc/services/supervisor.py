import logging
import subprocess
import configparser
import sys
import time
import os

from sc.config import SERVICES_DIR, SERVICES_LOG_DIR


LOG = logging.getLogger(__name__)


SUPERVISOR_CONFIG_PATH = SERVICES_DIR / 'supervisor.conf'


def _get_with_common_args(command, *args):
    cmd_args = [command]
    cmd_args += ['--configuration', SUPERVISOR_CONFIG_PATH]
    cmd_args += args

    return [os.fspath(arg) for arg in cmd_args]


def _display_cmd_args(args, printer=LOG.debug):
    as_str = str.join(' ', [os.fspath(arg) for arg in args])
    printer(f'Launching process: {as_str}')


def _get_subprocess_kwargs():
    d = {}

    d['env'] = dict(os.environ)
    d['env']['SC_INTERNAL_PROCESS'] = '1'

    return d


def _get_supervisord_args(*args):
    return _get_with_common_args('supervisord', *args)


def _get_supervisorctl_args(*args):
    return _get_with_common_args('supervisorctl', *args)


def _ensure_supervisord_is_running():
    test_cmd_args = _get_supervisorctl_args('pid')

    LOG.debug(f'Running command to test connection with supervisord process...')
    try:
        _display_cmd_args(test_cmd_args)
        proc = subprocess.run(test_cmd_args, stdout=subprocess.PIPE, check=True, text=True)
    except subprocess.CalledProcessError as e:
        LOG.info(
            'Could not connect to supervisord process.'
            ' This might mean that supervisord has not been started yet.'
        )
        LOG.info('Will attempt to start supervisord now.')
        _start_supervisord()
    else:
        daemon_pid = int(proc.stdout.rstrip())
        LOG.info(f'Connected to supervisord process (pid={daemon_pid})')


def _start_supervisord(timeout_s=2):
    cmd_args = _get_supervisord_args()

    LOG.info('Starting supervisord process...')
    _display_cmd_args(cmd_args)

    popen_opts = _get_subprocess_kwargs()
    if sys.platform == 'win32':
        popen_opts.update(
            creationflags=subprocess.CREATE_NEW_PROCESS_GROUP
        )
    proc = subprocess.Popen(cmd_args, **popen_opts)

    timeout_s = float(timeout_s)
    LOG.info(f'Waiting while the supervisord process initializes ({timeout_s} seconds)...')
    time.sleep(timeout_s)

    returncode = proc.poll()
    LOG.debug(f'returncode={returncode}')

    if returncode in {None, 0}:
        LOG.info(f'Successfully started supervisord process (pid={proc.pid})')
    else:
        LOG.error(f'Could not start supervisord process (error code={returncode}). Check error logs in {SERVICES_LOG_DIR} for more information.')
        sys.exit(1)


def _run_supervisorctl(*args, **kwargs):
    # at the moment we're not using this, but could come in handy if we want more capillary control on e.g. CTRL-C while in interactive mode
    is_interactive = None
    if args:
        LOG.info(f'services command: "{str.join(" ", args)}"')
        is_interactive = False
    else:
        LOG.info('No command given. An interactive session will be started.')
        is_interactive = True

    cmd_args = _get_supervisorctl_args(*args)

    if 'help' in cmd_args:
        LOG.debug('Assuming that this command does not need a connection to the supervisord process. The connection checks will be skipped.')
    else:
        _ensure_supervisord_is_running()

    try:
        _display_cmd_args(cmd_args)
        proc = subprocess.run(cmd_args, check=True, **_get_subprocess_kwargs())
        returncode = proc.returncode
        LOG.debug(f'supervisorctl return code={returncode}')
    except subprocess.CalledProcessError as e:
        returncode = e.returncode
        LOG.debug(f'supervisorctl return code={returncode}')
        if returncode == 1:
            LOG.error(f'supervisorctl exited with an error: {e.returncode}')
            # the exception at this stage is not super useful, since it's just the CalledProcess error
            # LOG.exception(e)
            sys.exit(1)
        elif returncode >= 2:
            # supervisorctl returns non-0, non-1 exit codes for a variety of reasons
            # it's probably easier to do deal with individual cases here
            if 'status' in args or 'start' in args:
                LOG.warning(
                    'One or more services are not running.'
                    ' This can mean that'
                    ' (A) they were not started yet,'
                    ' or (B) they were started, but an error occurred during startup.'
                    f' If (B), check the services logs in {SERVICES_LOG_DIR} for more information.'
                )
    except KeyboardInterrupt:
        LOG.info('Received keyboard interrupt. Exiting.')
        sys.exit(0)
