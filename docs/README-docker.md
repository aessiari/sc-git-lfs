## Using Science Capsule with Docker

### Installation

- These steps were successfully tested on:
    - Linux (Ubuntu 18.04.4 and 20.04)
    - macOS (10.12.6)
- At the moment, running a Science Capsule container on Windows is not supported (see issue #49 for additional information). This is being addressed and should become possible in the future. In the mean time, Science Capsule can be used on Windows by running it directly on the host OS ("bare-metal" setup).

#### System packages

- Git
- Docker and Docker Compose
    - Docker Compose should be included with Docker in most cases

#### Science Capsule repository

Clone the `sciencecapsule` repository and enter the `sciencecapsule` directory

```sh
git clone https://bitbucket.org/sciencecapsule/sciencecapsule
cd sciencecapsule
```
### Container workflow

#### Building a local Science Capsule Docker image

From the `docker` subdirectory of the repository,
run `docker-compose build` to build a base Science Capsule image locally:

```sh
cd docker/
docker-compose build capsule
```

Once the build step completes without errors, verify that the image has been created by running:

```sh
docker image ls
```

#### Creating and launching a Science Capsule container

The `run` subcommand command creates a running container from an existing image.
The `--name` can optionally be used to specify a human-readable name to assign to the container;
in this exampe, we'll use `my-capsule`.

From the same directory, run:

```sh
docker-compose run --name my-capsule capsule
```

The equivalent of the previous command using `docker` instead of `docker-compose` is:

```sh
docker run -it --name my-capsule sciencecapsule/base bash
```

When the container is started, all Science Capsule services are started automatically.
Once the container shell prompt appears, we are inside the container.

**NOTE** In order to view the workflow timeline through the web interface on the host system, port forwarding between the container and the host system should be setup during container creation and launch. Refer to [viewing the workflow timeline](webui.md) guide for details.

#### Enabling capture of process events using strace

**IMPORTANT**  Enabling `strace` within a Science Capsule container might require additional steps on the Docker host system. Refer to [this document](enabling-strace.md) for a detailed description.

Add the `--cap-add=SYS_PTRACE` and `--init` flags when starting the container with `docker run`:

```sh
docker run -it --name my-capsule --cap-add=SYS_PTRACE --init --publish 12345:54001 sciencecapsule/base bash
```

The PID of the starting process (in this example, `bash`) should be added automatically to the list of monitored PIDs before starting the Science Capsule services.

Verify that the process events are captured correctly by invoking an executable program (i.e. not a shell built-in), such as `ls`, and then inspect the event captured using one of the methods described above.

#### Stopping and restarting the container

By pressing CTRL-D, the container will be exited.
All running subprocesses, including the Science Capsule services, will be stopped as well.

To restart and re-enter the container, run:

```sh
docker start -ia my-capsule
```

The Science Capsule services will be restarted automatically.
Everything inside the container, including the events previously captured and analyzed by Science Capsule,
are in the same state as when the container was stopped.

#### Saving and exporting the container

The state of a container can be saved, exported, and transferred as a single tar archive.
The tar archive can then be loaded on any other Docker host.

Run `docker commit` to an image with the complete state of a stopped container:

```sh
docker commit my-capsule sciencecapsule/my-capsule:v1
```

Then, run `docker save` to export the image to a tar archive.
Use the same image label as in the previous step; the filename is not relevant and can be anything:

```sh
docker save sciencecapsule/my-capsule:v1 > my-capsule-v1.tar
```

The file, although relatively large, can be copied or transferred as normal,
e.g. using a removable drive, a cloud storage provider, SFTP/rsync, etc

#### Loading the exported image

On any Docker host, run the `docker load` command to convert back the tar archive into a Docker image:

```sh
docker load < my-capsule-v1.tar
```

Once the image is loaded, verify that it appears in `docker image ls`.
Then, run `docker run` to create and launch a container from the image.
The container ID will be different, but the state will be identical to the original container:

```sh
docker run -it --name my-capsule sciencecapsule/my-capsule:v1
```

At this point, the workflow will be identical to the steps described in the previous section.
