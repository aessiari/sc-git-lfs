## For Science Capsule running on bare-metal

The Science Capsule WebUI can be accessed at any time while the `webui_server` service is running.
The `sc services status` command can be used to display the status of all services.

The WebUI URL is `http://localhost:<PORT>`, where `<PORT>` is the port number set dynamically when running `sc bootstrap`, and might vary depending on available ports.
The base values are 54001 on Linux and macOS, and 54002 on Windows.
The exact value can be verified in the logs for the `webui_server` services accessible at `$SC_CONFIG_DIR/services/logs/webui_server.log`.

To access the WebUI, open its URL in a web browser (currently tested on Firefox and Chrome).


## For Science Capsule running inside Docker

To be able to access the WebUI from a browser running on the Docker host,
add the `-p/--publish` flag to the docker commands, specifying any free port on the host, e.g. `12345`,
and the WebUI port inside the container (by default, `54001`):

```sh
docker-compose run --name my-capsule --publish 12345:54001 capsule
docker run -it --name my-capsule --publish 12345:54001 sciencecapsule/base bash
```

As long as the Science Capsule container is running, the WebUI can be accessed with a web browser on `localhost`
using the host-side port specified when creating the container, as described above.
in this example, the complete URL would be `http://localhost:12345`.
