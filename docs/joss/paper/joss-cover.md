We would like to thank the reviewers for their detailed and insightful comments. Please find below the review comments and our response.


#### Documentation
1. This needs to be updated in the documentation:
```
git clone git@bitbucket.org:sciencecapsule/sciencecapsule
Cloning into 'sciencecapsule'...
git@bitbucket.org: Permission denied (publickey).
fatal: Could not read from remote repository.
Please make sure you have the correct access rights
and the repository exists.
```
The below worked --
```
git clone https://bitbucket.org/sciencecapsule/sciencecapsule.git 
``` 

*Response*: 
For cloning the git repository through ssh, users have to ensure that their public key is added into their git account. We have added this requirement in the readme. We have aslo added text suggesting https as the preferred way of cloning the repository. 

2. Link for webui: https://bitbucket.org/sciencecapsule/sciencecapsule/src/5aae0611fdf23a34df444e3c7fb6dc3fba67614a/docs/docs/webui.md in https://bitbucket.org/sciencecapsule/sciencecapsule/src/5aae0611fdf23a34df444e3c7fb6dc3fba67614a/docs/example.md
doesn’t work.

*Response*: We have fixed the broken links in the documentation.


3. Docker approach is very well documented. For 'bare metal' you could also add "Saving and exporting the provenance" or something like that.

*Response*: All the provenance and workflow execution events collected by Science Capsule is internally stored in a mongodb database. The web UI uses the database to show the timeline view of these events. Additionally, users can use the `sc inspect` command to export the events stored in the mongodb. We have added text in the [documentation](https://bitbucket.org/sciencecapsule/sciencecapsule/src/master/docs/example.md). We are working on better ways to export and represent the provenance information stored in the database.

4. I have identified problems with the Windows instructions and docker.
   
*Response*: The issue identifies that Science Capsule does not work on Windows ([issue#49](https://bitbucket.org/sciencecapsule/sciencecapsule/issues/49/readmemd-not-working-properly)). However, the actual issue is using Science Capsule on Windows with a docker container. In the readme for docker, we now explicitly mention that the docker support is only tested on Linux and Mac, and not on Windows. We will fix this issue in our future releases.

#### Software
5. Shouldn't the conda env sc be activated by default in the Docker image? You could use RUN echo "conda activate sc" >> ~/.bashrc in the Dockerfile for that for example.
   
*Response*: `conda activate sc` is required only when running Science Capsule in bare-metal mode. When running it using containers, there is no need to activate the conda environment as the sc command is added to the $PATH variable.


6. Is there a contributors guide?

*Response*: We do not have a contributors guide right now, but we will add one with the next release (coming soon).

#### Paper
7. The paper needs a major revision. There are repeated statements, inconsistent use of "data processing", "analysis", "workflow" (choose one). I suggest adding more information on the implementation (ie., you use Python and Node JS, but that’s not mentioned).
   
*Response*: We have addressed the concerns and fixed the text accordingly. We now use "scientific workflows" instead on data processing and/or analyses and/or workflows. 

8. My questions while reading the paper:
- Is the tool currently used? How was it evaluated?
- What is the output of the app? How is it shared?
- Is any future work planned?
- Edit (added after testing with Docker): You should add in the paper why one should use SC instead of using Docker directly? Most of the commands for this functionality are docker commands.

*Response*: We have added text addressing all the above issues in Section-2 and Section-3. For the docker usage, the docker commands are used only to start and manage the containers. However, the containers themselves have Science Capsule installed that transparently monitors and captures the metadata necessary to understand and reproduce a workflow.

9. Here are some line-by-line comments (but a major revision is advised):
    
- L6: Sentence seems incomplete and vague. I suggest expanding it into 2-3 sentences.
- L7: I would remove "sharing".
- L7: code scripts?
- L8: What is context?
- L11: Aren't "end-to-end data processing" and "analysis - life cycle" typically the same thing? Maybe choose one. I suggest removing 'life cycle' here - that typically includes preservation/reuse.
- L13-14: Unclear sentence; I suggest expanding it into 2-3 sentences.
- L15-16: What applications? Code scripts?
- L16: "portability" instead of "sharing"
- L17: maybe "re-execution"?
- L19: remove "environments"?
- L20-27: I suggest making a paragraph out of this that will make it read nicely (and removing repeated statements i.e., L20,21)
- L24: what workflow? Use already introduced terms, ie data processing or analysis.
- L29-31: repeated statement, I suggest removing it.
- L31-32: feels a bit empty...
- L36-39: strong paragraph! I suggest merging it with the previous one and have a unified message (try not to repeat what was said in Summary.
- L40: Maybe 1-2 sentences more info about the differences and use. What are the outputs for each mode?
- L67: There should be more discussion about the difference between SC and reproZip and SciUnit, which are similar.
- L79: broken reference
- L81: broken reference

*Response*: We have addressed the concerns in the paper. We have fixed the text removing any repetitions and/or ambiguity. We have discussed the differences between SC, reprozip and Sciunit in related work. We have also cross-checked the references and they seem to work correctly. Let us know if that's not the case.


Thank you.

